'use strict';

//= ../node_modules/jquery/dist/jquery.min.js
//= ../node_modules/lazysizes/lazysizes.js
//= ../node_modules/swiper/swiper-bundle.js
//= ../node_modules/@fancyapps/fancybox/dist/jquery.fancybox.js




$(document).ready(function () {

	// подключение наших компонентов
	//= components/input-select.js

	$(".form-control_password .form-control__btn-view").on("click", function() {
		let parent = $(this).closest(".form-control_password");

		if (  parent.find("input").attr("type") == "password" ) {
			parent.find("input").attr("type", "text");
			$(this).addClass("form-control__btn-view_hidden");
		} else {
			parent.find("input").attr("type", "password");
			$(this).removeClass("form-control__btn-view_hidden");
		}
	});


	// слайдер на первом экране с dots
	new Swiper(".welcome-slider", {
		pagination: {
		  el: ".swiper-pagination_welcome",
		},
	});

	// стандартный слайдер
	new Swiper(".default-slider", {
        slidesPerView: 1,
        spaceBetween: 10,
        breakpoints: {
          640: {
            slidesPerView: 2,
            spaceBetween: 20,
          },
          768: {
            slidesPerView: 3,
            spaceBetween: 40,
          },
          1024: {
            slidesPerView: 3,
            spaceBetween: 40,
          },
        },
	});
});
